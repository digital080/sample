// Example program

#include <iostream>
#include <random>
#include <vector>

using namespace std;

int main()
{
    std::random_device rd;
    std::mt19937 gen(rd());
    
    float mean = 50; // set population mean
    float std_dev = 6;   //set standard deviation
    
    int n = 20; //set sample size
    float num = 0;
    float total = 0;
    vector <float> ran;
    vector <float> avg;
    
    
    std::normal_distribution<float> norm(mean,std_dev); //setup distribution object
 
    

    
    for(int i = 0 ; i < 10; i++){// create a sample of size 10
		ran.clear();
		for(int i = 0 ; i < 10; i++){// create a sample of size 10
       //cout<< norm(gen)<<endl; // print out a random number to the screen 
       num = norm(gen);
       ran.push_back(num);
		}
		
		
		for(int x = 0 ; x < 10; x++){
		//cout<< ran[x] <<endl; // print out a random number to the screen 
		total += ran[x];
		}
		

		
		num = total/10;
		avg.push_back(num);
		
		 num = 0;
		 total = 0;
		
    }
    
    
    for(int i = 0 ; i < 10; i++){
    cout << "Mean of each: " << avg[i] << endl;
	}
	
	
    for(int i = 0 ; i < 10; i++){
		
    cout << "Interval Positive: " << avg[i]+(1.96 * 6 / sqrt(20)) <<  " Interval Negative: " << avg[i]-(1.96 * 6 / sqrt(20)) << endl;
    cout << "Interval: " << avg[i]+(1.96 * 6 / sqrt(20)) - avg[i]+(1.96 * 6 / sqrt(20)) << endl;
	}
	
    //To Do: Create correct number of samples
    //       Calculate means for each sample
    //       (Calculate confidence intervals also??)
        
        
}
